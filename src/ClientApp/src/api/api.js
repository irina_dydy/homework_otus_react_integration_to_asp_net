import axios from 'axios';
import Config from '../Config';



export const getQuery=  async (props)=>{
    return await axios.get(`${Config.BASE_URL}/${props}`)
            .catch((e)=>console.error(`GET: Error get ${props} query: ${e}`));
}